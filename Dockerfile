FROM golang:1.16-buster AS builder

WORKDIR /src/app

# install system dependencies
RUN apt-get update \
  && apt-get -y install netcat \
  && apt-get clean

COPY go.* ./
RUN go mod download
COPY . .

ARG TARGETOS
ARG TARGETARCH

RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH}  go build -o dblab .

FROM scratch AS bin

LABEL org.opencontainers.image.documentation="https://gitlab.com/osstorres/golab" \
	org.opencontainers.image.source="https://gitlab.com/osstorres/golab" \

COPY --from=builder /src/app/golab /bin/golab
