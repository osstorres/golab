package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/tkanos/gonfig"
)

type Configuration struct {
	Project     string
	AccessToken string
	AssigneeId  int
	Branches    []string
}

func GetConfig(params ...string) Configuration {
	configuration := Configuration{}
	fileName := "./gitlabconfig.json"
	gonfig.GetConf(fileName, &configuration)
	return configuration
}

type branches []string

func (h *branches) String() string {
	return fmt.Sprint(*h)
}

func (h *branches) Set(value string) error {
	for _, branch := range strings.Split(value, ",") {
		*h = append(*h, branch)
	}
	return nil
}
func main() {

	var branchesFlag branches
	flag.Var(&branchesFlag, "branches", "Comma separated list of target branch")
	projectID := flag.Int("project", 0, "Specify your project")
	issueName := flag.String("issue", "default", "Specify your issue name")
	// Enable command-line parsing
	flag.Parse()
	flag.Parse()
	commandLab := flag.Arg(0)
	issueTemp := strings.Replace(*issueName, " ", "-", -1)
	issueNameFinal := strings.ToLower(issueTemp)
	fmt.Println("Command lab: ", commandLab)
	fmt.Println("project ID: ", *projectID)
	fmt.Println("Issue Name: ", issueNameFinal)
	fmt.Printf("Your branches are: ")
	for _, branch := range branchesFlag {
		fmt.Printf("%s ", branch)
	}

	gitlabConfiguration := GetConfig()

	baseUrlGitlab := fmt.Sprintf("https://gitlab.com/api/v4/projects/%s/", gitlabConfiguration.Project)
	urlIssue := baseUrlGitlab + "issues/?access_token=" + gitlabConfiguration.AccessToken
	urlMr := baseUrlGitlab + "merge_requests/?access_token=" + gitlabConfiguration.AccessToken
	urlBranch := baseUrlGitlab + "repository/branches/?access_token=" + gitlabConfiguration.AccessToken

	method := "POST"

	var issue_name string

	fmt.Print("Issue name: ")
	fmt.Scan(&issue_name)
	payload_str := fmt.Sprintf(`{"title": "%s",
	"assignee_id": %d,
	"labels": "back",
	"milestone_id": 22222}`, issue_name, gitlabConfiguration.AssigneeId)
	payload := strings.NewReader(payload_str)
	client := &http.Client{}
	req, err := http.NewRequest(method, urlIssue, payload)

	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(body))

	// branch
	payloadBranch := strings.NewReader(`{"branch": "test0002", "ref": "main" }`)
	reqBranch, errBranch := http.NewRequest(method, urlBranch, payloadBranch)

	if errBranch != nil {
		fmt.Println(errBranch)
		return
	}
	reqBranch.Header.Add("Content-Type", "application/json")

	resBranch, errBranch := client.Do(reqBranch)
	if errBranch != nil {
		fmt.Println(errBranch)
		return
	}
	defer resBranch.Body.Close()

	bodyBranch, errBranch := ioutil.ReadAll(resBranch.Body)
	if errBranch != nil {
		fmt.Println(errBranch)
		return
	}
	fmt.Println(string(bodyBranch))
	// merge request

	var mergeName string

	fmt.Print("Merge name: ")
	fmt.Scan(&mergeName)
	payloadMergeStr := fmt.Sprintf(`{"source_branch": "test0002",
	"target_branch": "main",
	"title": "%s",
	"assignee_id": %d,
	"remove_source_branch": true,
	"squash": true }`, mergeName, gitlabConfiguration.AssigneeId)
	payloadMerge := strings.NewReader(payloadMergeStr)
	reqMerge, errMerge := http.NewRequest(method, urlMr, payloadMerge)

	if errMerge != nil {
		fmt.Println(errMerge)
		return
	}
	reqMerge.Header.Add("Content-Type", "application/json")

	resMerge, errMerge := client.Do(reqMerge)
	if errMerge != nil {
		fmt.Println(errMerge)
		return
	}
	defer resMerge.Body.Close()

	bodyMerge, errMerge := ioutil.ReadAll(resMerge.Body)
	if errMerge != nil {
		fmt.Println(errMerge)
		return
	}
	fmt.Println(string(bodyMerge))
}
