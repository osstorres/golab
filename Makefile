PLATFORM=linux/amd64

.PHONY: test
## test: Runs the tests
test:
	go test -v -race ./...

.PHONY: unit-test
## unit-test: Runs the tests with the short flag
unit-test:
	go test -v -short -race ./...

.PHONY: int-test
## int-test: Runs the integration tests
int-test:
	docker-compose run --entrypoint=make golab test

.PHONY: linter
## linter: Runs the golangci-lint command
linter:
	golangci-lint run --enable=golint --enable=godot --enable=gofmt ./...

.PHONY: test-all
## test-all: Runs the integration testing bash script with different database docker image versions
test-all:
	@./scripts/test_all.sh

.PHONY: docker-build
## docker-build: Builds de Docker image
docker-build:
	@docker build --target bin --output bin/ --platform ${PLATFORM} -t golab .

.PHONY: build
## build: Builds the Go program
build:
	CGO_ENABLED=0 \
	go build -o golab .

.PHONY: run
## run: Runs the application
run: build
	./golab --host localhost --user postgres --db users --pass password --ssl disable --port 5432 --driver postgres

.PHONY: run-mysql
## run-mysql: Runs the application with a connection to mysql
run-mysql: build
	./golab --host localhost --user myuser --db mydb --pass 5@klkbN#ABC --ssl enable --port 3306 --driver mysql

.PHONY: run-sqlite3
## run-sqlite3: Runs the application with a connection to sqlite3
run-sqlite3: build
	docker-compose run --rm golab-sqlite3
	./golab --db db/golab.db --driver sqlite3

.PHONY: help
## help: Prints this help message
help:
	@echo "Usage:"
	@sed -n 's/^##//p' ${MAKEFILE_LIST} | column -t -s ':' |  sed -e 's/^/ /'
