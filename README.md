
<p>
  <img style="float: right;" src="go.png" alt="dblab logo"  width=100>
</p>


__Interactive client for Gitlab WIP.__




## Overview



## Features

- Cross-platform support OSX/Linux/Windows 32/64-bit
- Simple installation (distributed as a single binary)
- Zero dependencies

## Installation

### Homebrew


### Binary Release (Linux/OSX/Windows)

## Help

```


Usage:
  golab [flags]
  golab [command]

Available Commands:
  help        Help about any command
  version     The version of the project

Flags:
      --create-mr          get the connection data from a config file (default is $HOME/.dblab.yaml or the current directory)
      --create-issue       Database name

Use "golab [command] --help" for more information about a command.
```

## Usage


### Key Bindings

## Contribute


## License
